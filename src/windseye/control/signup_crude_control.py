#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Crescent
# Copyright 2013-2015 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
#
# Crescent é um software livre; você pode redistribuí-lo e/ou
# modificá-lo dentro dos termos da Licença Pública Geral GNU como
# publicada pela Fundação do Software Livre (FSF); na versão 2 da
# Licença.
#
# Este programa é distribuído na esperança de que possa ser útil,
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
# a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em <http://www.gnu.org/licenses/>

"""Entrypoint for web requests.
.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>
"""
from bottle import route, view, request
import bottle

appbottle = bottle.default_app


@bottle.get('/signup/show')
@view('signup')
def show_signup_crude_form():
    return {}


@route('/signup/sign_up')
@view('signup')
def sign_up_register_a_new_dance_user():

    return {}


@route('/signup/forgot')
@view('forgot')
def forgot_password_forward_reset_email():

    return {}


@route('/signup/help')
@view('help')
def help():

    return {}


@route('/signup/sign_in')
@view('login')
def sign_in_form_to_collect_user_authentication():

    return {}


@bottle.post('/signup/login')
@view('index')
def login():
    username, password = request.forms.get("username"), request.forms.get("password")
    return dict(signer="aluno", isigner="sign-out-alt", sign_="auth/sign_out", splash='')

