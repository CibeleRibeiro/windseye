
from bottle import run, TEMPLATE_PATH, static_file, route, default_app, error, view

from . import tpl_dir
from . import static_control
from . import login_control
# from . import adm_crude_control
# from . import signup_crude_control

# Create a new list with absolute paths
# TEMPLATE_PATH.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../view/tpl')))
# make sure the default templates directory is known to Bottle

if tpl_dir not in TEMPLATE_PATH:
    TEMPLATE_PATH.insert(0, tpl_dir)


@route('/')
@view('index')
def index():
    return dict(signer="Sign In", isigner="sign-in-alt", sign_="auth/sign_in", splash='is-active')


@error(404)
@view('sorry')
def error404(_=0):
    return {}


application = default_app()
_ = application

# Mount a new instance of bottle for each controller and URL prefix.
# appbottle.mount("/external/brython/Lib/site-packages", project_controller.bottle)
# application.mount("/<:re:.*>/_spy", code_controller.bottle)
application.mount("/<:re:.*>/js", static_control.appbottle)
application.mount("/<:re:.*>/img", static_control.appbottle)
application.mount("/<:re:.*>/css", static_control.appbottle)
application.mount("/<:re:.*>/_static", static_control.appbottle)
application.mount("/<:re:.*>/auth", login_control.appbottle)
# application.mount("/<:re:.*>/adm", adm_crude_control.appbottle)
# application.mount("/<:re:.*>/signup", signup_crude_control.appbottle)

if __name__ == "__main__":
    run(host='localhost', port=8080)
