#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Crescent
# Copyright 2013-2015 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
#
# Crescent é um software livre; você pode redistribuí-lo e/ou
# modificá-lo dentro dos termos da Licença Pública Geral GNU como
# publicada pela Fundação do Software Livre (FSF); na versão 2 da
# Licença.
#
# Este programa é distribuído na esperança de que possa ser útil,
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
# a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em <http://www.gnu.org/licenses/>

"""Entrypoint for web requests.
.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>
"""
from bottle import route, view, request
import bottle

appbottle = bottle.default_app


@bottle.get('/adm/show')
@view('adm')
def show_crude_form():
    return dict(T1="Bailes", T2="Danças", T3="Professores", T4="Ofertas")


@route('/adm/sign_up')
@view('signup')
def sign_up_register_a_new_dance_user():

    return {}


@route('/adm/forgot')
@view('forgot')
def forgot_password_forward_reset_email():

    return {}


@route('/adm/help')
@view('help')
def help():

    return {}


@route('/adm/sign_in')
@view('login')
def sign_in_form_to_collect_user_authentication():

    return {}


@bottle.post('/adm/login')
@view('index')
def login():
    username, password = request.forms.get("username"), request.forms.get("password")
    return dict(signer="aluno", isigner="sign-out-alt", sign_="auth/sign_out", splash='')

