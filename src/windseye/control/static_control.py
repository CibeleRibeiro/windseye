from bottle import get, static_file, default_app
from . import img_dir
from . import css_dir
from . import js_dir
appbottle = default_app



@get("/_static/<filepath:re:.*\.(png|jpg|svg|gif|ico|jpeg)>")
def img(filepath):
    return static_file(filepath, root=img_dir)


@get("/css/<filepath:re:.*\.css>")
def ajs(filepath):
    return static_file(filepath, root=css_dir)


@get("/js/<filepath:re:.*\.js>")
def ajs(filepath):
    return static_file(filepath, root=js_dir)


