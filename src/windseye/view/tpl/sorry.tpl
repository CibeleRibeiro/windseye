  <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Help - Clube Da Dança</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet"/>
    <!-- Bulma Version 0.7.1-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/login.css"/>
        <link rel="stylesheet" type="text/css" href="../css/is-awesome.css"/>
        <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet"/>
</head>

<body>

		<section class="hero is-light is-large ">
		  <div class="hero-body">
		  	<div class="container">
				<div class="columns">
					<div class="column is-3 is-offset-2">
                        <figure class="avatar">
                            <img src="/_static/cant-clipart-5.jpg"/>
                        </figure>

		    			<h1 class="title" style="font-size: 70px;color:#ff470f">404</h1>
						<h2 class="subtitle is-3">Page not found</h2>
					</div>
					<div class="column is-5">
						<p>We could not find the page you were looking for. You can return to <a href="/">dashboard</a> or use the search form.</p>
						<br/>
						<p class="control has-icon has-icon-right">
						  	<input class="input" placeholder="Search"/>
						  	<span class="icon is-small">
                                <i class="fa fa-search"></i>
						  	</span>
						</p>
					</div>
				</div>
		    </div>
		  </div>
		</section>
    <script async="" type="text/javascript" src="../js/bulma.js"></script>
</body>

</html>