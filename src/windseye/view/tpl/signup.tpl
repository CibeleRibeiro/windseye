  <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login - Clube Da Dança</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Bulma Version 0.7.1-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/login.css">
    <script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
    <script
  src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js">
    </script>
    <script>
        var cnpjcpf= $("#cpfcnpj").val().length;

    if(cnpjcpf < 11){
        $("#cpfcnpj").mask("999.999.999-99");
    } else {
        $("#cpfcnpj").mask("99.999.999/9999-99");
    }
    </script>
</head>

<body>
    <section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <h3 class="title has-text-yellow">Registro</h3>
                    <p class="subtitle has-text-yellow">Complete o cadastro com seus dados.</p>
                    <div class="box">
                        <figure class="avatar">
                            <img src="/_static/primeiro.jpeg">
                        </figure>
                        <form method="post" action="/login">
                            <div class="field">
                                <div class="control">
                                    <input name="name" class="input is-large" type="text" placeholder="Nome" autofocus="">
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input name="surname" class="input is-large" type="text" placeholder="Sobrenome" autofocus="">
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <select name="membership" class="input is-large">
                                        <option value="">--Escolha tipo de sociedade--</option>>
                                        <option value="client">Sócio Cliente</option>
                                        <option value="dance_studio">Sócio Espaço de Dança</option>
                                        <option value="event">Sócio Evento</option>
                                    </select>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input name="birth" class="input is-large" type="date" placeholder="Data de nascimento">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="email" class="input is-large" type="email" placeholder="Email" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="CPF/CNPJ" class="input is-large" type="text" id="cpfcnpj" placeholder="CPF/CNPJ" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="telefone" class="input is-large" type="tel" placeholder="Telefone" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="celular" class="input is-large" type="tel" placeholder="Celular" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="CEP" class="input is-large" type="text" placeholder="CEP" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="UF" class="input is-large" type="text" placeholder="UF" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="cidade" class="input is-large" type="text" placeholder="Cidade" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="bairro" class="input is-large" type="text" placeholder="Bairro" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="numero" class="input is-large" type="number" placeholder="Número" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="complemento" class="input is-large" type="text" placeholder="Complemento" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="username" class="input is-large" type="email" placeholder="Usuário" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="senha" class="input is-large" type="password" placeholder="Senha" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="confirmar senha" class="input is-large" type="password" placeholder="Confirmar senha" autofocus="">
                                </div>
                            </div>
                            <button class="button is-block is-info is-large is-fullwidth">Enviar</button>
                        </form>
                    </div>
                    <p class="has-text-yellow">
                         <a href="help">Precisa de ajuda?</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <script async type="text/javascript" src="../js/bulma.js"></script>
</body>

</html>
