<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Clube da Dança</title>
    <link rel="shortcut icon" href="../images/fav_icon.png" type="image/x-icon"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet"/>
    <!-- Bulma Version 0.7.1-->
    <link rel="stylesheet" href="../css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/landing.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"/>

<script>
    function close(){
        var NAME = document.getElementById("the_modal");
        NAME.classList.toggle('is-active');}
</script>
    <script  type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {

            // Get all "navbar-burger" elements
            var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            // Check if there are any navbar burgers
            if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach(function ($el) {
              $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                var target = $el.dataset.target;
                var $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

              });
            });
            }

        });
    </script>    <!-- fonts -->
</head>

<body>
    <div id="the_modal" class="modal {{splash}}" onclick="close()">
        <div class="modal-background">
        </div>
            <div class="modal-content">
                <img width="500" src="/_static/primeiro.jpeg" onclick="close()"/>
            </div>
            <button class="modal-close is-large" aria-label="close" onclick='document.getElementById("the_modal").classList.toggle("is-active");'></button>

    </div>
    <section class="hero is-info is-fullheight">
        <div class="hero-head">
            <nav class="navbar">
                <div class="container">
                    <div class="navbar-brand">
                        <a class="navbar-item" href="../">
                            <img src="/_static/keylogooficial3.png" alt="Logo"/>
                        </a>
                        <span class="navbar-burger burger" data-target="navbarMenu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </div>
                    <div id="navbarMenu" class="navbar-menu">
                        <div class="navbar-end">
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="#expanda">
                                    <span class="icon">
                                        <i class="fas fa-home"></i>
                                    </span>
                                    <span>HOME</span>
                                </a>
                            </span>
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="#noticias">
                                    <span class="icon">
                                        <i class="far fa-flag"></i>
                                    </span>
                                    <span>SOBRE</span>
                                </a>
                            </span>
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="#noticias">
                                    <span class="icon">
                                        <i class="fas fa-shoe-prints"></i>
                                    </span>
                                    <span>AULAS</span>
                                </a>
                            </span>
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="#noticias">
                                    <span class="icon">
                                        <i class="fas fa-boxes"></i>
                                    </span>
                                    <span>PACOTES</span>
                                </a>
                            </span>
                            <span class="navbar-item">
                                <a class="button is-white is-outlined" href="/{{sign_}}">
                                    <span class="icon">
                                        <i class="fas fa-user-friends"></i>
                                    </span>
                                    <span>ÁREA DO SÓCIO</span>
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </nav>
            </div>

            <div class="hero-body">
                <div class="container has-text-centered">
                    <div class="column is-6 is-offset-3">
                        <h1 class="title">
                            <br/>
                            Clube da Dança
                        </h1>
                        <h2 class="subtitle">
                            Quer dançar? Com apenas alguns cliques, opte pelo melhor pacote para escolher onde e quando dançar sem precisar estar preso a uma só academia.  Cadastre-se para abrir novos horizontes no mundo da dança.
                        </h2>

                    </div>
                </div>
            </div>

    </section>

        <div class="section">
            <!-- Developers -->
            <div class="hero-body">
                <div class="container has-text-centered">
                    <h1 class="title">Novos Horizontes</h1>
                    <a id="expanda" name="expanda"></a>
                </div>
            </div>
            <div class="row columns">
                <div class="column is-one-fourth">
                    <div class="card large">
                        <div class="card-image" style="height:300px; overflow:hidden" >
                            <figure class="image">
                                <img src="/_static/ballerina-2878011_1920.jpg" alt="Academias"/>
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">

                                <div class="media-content">
                                    <p class="title is-4 no-padding">Academias</p>
                                    <p><span class="title is-6"><a href=""></a></span></p>
                                    <p class="subtitle is-6"></p>
                                </div>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae, quas accusantium perferendis sapiente explicabo, corporis totam! Labore reprehenderit beatae magnam animi!
                                <div class="background-icon"><span class="icon-twitter"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-one-fourth">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="/_static/day-planner-828611_1920.jpg" style="height:300px" alt="Horários"/>
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4 no-padding">Horários</p>
                                    <p><span class="title is-6"><a href=""></a></span></p>
                                    <p class="subtitle is-6"></p>
                                </div>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae, quas accusantium perferendis sapiente explicabo, corporis totam! Labore reprehenderit beatae magnam animi!
                                <div class="background-icon"><span class="icon-facebook"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-one-fourth">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="/_static/ballet-999802_1920.jpg" style="height:300px" alt="Professor"/>
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4 no-padding">Professor</p>
                                    <p><span class="title is-6"><a href=""></a></span></p>
                                    <p class="subtitle is-6"></p>
                                </div>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae, quas accusantium perferendis sapiente explicabo, corporis totam! Labore reprehenderit beatae magnam animi!
                                <div class="background-icon"><span class="icon-barcode"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="column is-one-fourth">
                    <div class="card large">
                        <div class="card-image">
                            <figure class="image">
                                <img src="/_static/argentine-tango-2079964_1920.jpg" style="height:300px" alt="Modulos"/>
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4 no-padding">Modulos</p>
                                    <p><span class="title is-6"><a href=""></a></span></p>
                                    <p class="subtitle is-6"></p>
                                </div>
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum consequatur numquam aliquam tenetur ad amet inventore hic beatae, quas accusantium perferendis sapiente explicabo, corporis totam! Labore reprehenderit beatae magnam animi!
                                <div class="background-icon"><span class="icon-barcode"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Developers -->
            <div class="hero-body">
                <div class="container has-text-centered">
                    <h1 class="title">Cortesias</h1>
                    <a id="noticias" name="noticias"></a>
                </div>
            </div>

            <!-- Staff -->
            <div id="people" class="row columns is-multiline"></div>
            <!-- End Staff -->
        </div>
            <section class="hero is-info is-medium is-bold">
    </section>

    <script async type="text/javascript" src="../js/bulma.js"></script>
</body>

</html>