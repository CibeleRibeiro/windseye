  <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Login - Clube Da Dança</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet"/>
    <!-- Bulma Version 0.7.1-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/login.css"/>
</head>

<body>
    <section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <div class="box">
                        <figure class="avatar">
                            <img src="/_static/primeiro.jpeg">
                        </figure>
                        <form method="post" action="/auth/login">
                            <div class="field">
                                <h3 class="title has-text-grey">Login</h3>
                                <p class="subtitle has-text-grey">Efetue o login para acessar a Área do Sócio</p>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input name="username" class="input is-large" type="email" placeholder="Email" autofocus=""/>
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input name="password" class="input is-large" type="password" placeholder="Senha"/>
                                </div>
                            </div>
                            <div class="field">
                                <label class="checkbox">
                  <input type="checkbox">
                      Lembrar-se de mim</input>
                </label>
                            </div>
                            <button class="button is-block is-info is-large is-fullwidth">Login</button>
                            </div>
                        </form>
                    </div>
                    <p class="has-text-yellow">
                        <a href="sign_up">Cadastre-se</a> &nbsp;·&nbsp;
                        <a href="forgot">Esqueçeu sua senha?</a> &nbsp;·&nbsp;
                        <a href="help">Precisa de ajuda?</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <script async type="text/javascript" src="../js/bulma.js"></script>
</body>

</html>