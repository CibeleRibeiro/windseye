<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Área do Sócio</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet"/>
    <!-- Bulma Version 0.7.1-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css"/>
    <link rel="stylesheet" type="text/css" href="../css/area-do-socio.css"/>
</head>

<body>
<section class="hero is-success is-fullheight">
    <div class="hero-body">
        <div class="container  has-text-centered is-centered">
            <div class="box column is-10 is-offset-1">
                <div class="hero column is-2 is-offset-5">
                    <figure class="avatar">
                                <img src="/_static/primeiro.jpeg"/>
                    </figure>
                </div>
                <div class="container">
                    <div class="container columns has-text-centered">
                        <div class="column is-5">
                                <button class="button is-block is-info is-large is-fullwidth">Login</button>
                        </div>
                        <div class="column is-5">
                                <button class="button is-block is-info is-large is-fullwidth">Login</button>
                        </div>
                    </div>
                    <div class="container columns has-text-centered">
                        <div class="column is-5 is-centered">
                                <button class="button is-block is-info is-large is-fullwidth">Login</button>
                        </div>
                        <div class="column is-5 is-centered">
                                <button class="button is-block is-info is-large is-fullwidth">Login</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script async type="text/javascript" src="../js/bulma.js"></script>
        </body>

        </html>