Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_.


`Unreleased`_
-------------

Added
+++++
- expected look only version.


`20.04`_ 
--------



Added
+++++
- first draft.


-------

UTOP Consultoria Educacional
-------------------------------------------------

**Copyright © Cibele Ribeiro**


.. ..


 [logo]: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png "Laboratório de Automação de sistemas Educacionais"
 <!---

.. ..


.. image:: src/windseye/_static/windseye.png
   :alt: UTOP Consultoria Educacional



.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/


.. ..

 --->

.. ..